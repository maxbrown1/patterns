<?php

namespace App\Http\Action\Patterns\Creational;

use App\Http\JsonResponse;
use App\Services\Patterns\Creational\FactoryMethod\FactoryMethodService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class FactoryMethodAction implements RequestHandlerInterface
{
    /**
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $attributes = $request->getQueryParams();

        $superhero = $attributes['superhero'] ?? null;

        $abilitySuperHero = (new FactoryMethodService($superhero))->getAbility();

        return new JsonResponse($abilitySuperHero);
    }
}