<?php

namespace App\Http\Action\Patterns\Creational\AbstractFactory;

use App\common\Exception\BadParameterException;
use App\Http\JsonResponse;
use App\Services\Patterns\Creational\AbstractFactory\Exception\AbstractFactoryException;
use App\Services\Patterns\Creational\AbstractFactory\ServiceManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class AbstractFactorySubjectAction implements RequestHandlerInterface
{
    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws BadParameterException|AbstractFactoryException
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $attributes = $request->getQueryParams();

        $transport = $attributes['transport'] ?? null;

        if (!is_string($transport)) {
            throw new BadParameterException();
        }

        $service = (new ServiceManager($transport))->getService();

        return new JsonResponse([
            'subject' => $service->transportSubject()->subject()
        ]);
    }
}