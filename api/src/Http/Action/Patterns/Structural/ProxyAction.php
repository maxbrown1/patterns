<?php

declare(strict_types=1);

namespace App\Http\Action\Patterns\Structural;

use App\Http\JsonResponse;
use App\Services\Patterns\Structural\Proxy\ProxyTimeService;
use App\Services\Patterns\Structural\Proxy\Time;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ProxyAction implements RequestHandlerInterface
{
    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        return new JsonResponse(
            [
                (new ProxyTimeService(new Time()))->time()
            ]
        );
    }
}

