<?php

declare(strict_types=1);

namespace App\Http\Action\Patterns\Structural;

use App\Http\JsonResponse;
use App\Services\Patterns\Structural\Adapter\RadioStation;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class AdapterAction implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        return new JsonResponse([
            'play' => (new RadioStation())->play()
        ]);
    }
}
