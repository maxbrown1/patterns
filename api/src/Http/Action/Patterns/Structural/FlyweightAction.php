<?php

declare(strict_types=1);

namespace App\Http\Action\Patterns\Structural;

use App\Http\JsonResponse;
use App\Services\Patterns\Structural\Flyweight\HeroService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class FlyweightAction implements RequestHandlerInterface
{
    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        return new JsonResponse(
            [
                'hero' => (new HeroService())->execute()
            ]
        );
    }
}
