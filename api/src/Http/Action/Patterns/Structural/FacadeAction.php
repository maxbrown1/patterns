<?php

declare(strict_types=1);

namespace App\Http\Action\Patterns\Structural;

use App\Http\JsonResponse;
use App\Services\Patterns\Structural\Facade\FacadeService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class FacadeAction implements RequestHandlerInterface
{
    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     *
     * @throws \Exception
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $data = $request->getQueryParams()['text'];

        return new JsonResponse((new FacadeService($data))->facadeRun());
    }
}