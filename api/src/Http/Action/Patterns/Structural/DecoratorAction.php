<?php

declare(strict_types=1);

namespace App\Http\Action\Patterns\Structural;

use App\Http\JsonResponse;
use App\Services\Patterns\Structural\Decorator\BeerBottle;
use App\Services\Patterns\Structural\Decorator\BottleDecorator;
use App\Services\Patterns\Structural\Decorator\NukaColaBottle;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class DecoratorAction implements RequestHandlerInterface
{

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        return new JsonResponse([
            'Beer' => (new BottleDecorator(new BeerBottle()))->drink(),
            'NukaCola' => (new BottleDecorator(new NukaColaBottle()))->drink()
        ]);
    }
}
