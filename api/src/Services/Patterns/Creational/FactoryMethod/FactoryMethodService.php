<?php

declare(strict_types=1);

namespace App\Services\Patterns\Creational\FactoryMethod;

use App\Helpers\TextHelper;

class FactoryMethodService
{
    private const PATH_CLASS = 'App\Services\Patterns\Creational\FactoryMethod\Superheroes\\';

    private ?string $superhero;

    public function __construct(?string $superhero)
    {
        $this->superhero = $superhero;
    }

    /**
     * @return string[]
     */
    public function getAbility(): array
    {
        if (!$this->superhero) {
            return self::noHero();
        }

        $className = ucfirst(TextHelper::snakeToCamel($this->superhero));

        $class = self::PATH_CLASS . $className;

        if (!class_exists($class)) {
            return self::noHero();
        }

        return (new $class())->ability();
    }

    /**
     * @return string[]
     */
    private function noHero(): array
    {
        return [
            'no hero, no power'
        ];
    }
}
