<?php

declare(strict_types=1);

namespace App\Services\Patterns\Creational\FactoryMethod;

interface Superhero
{
    /**
     * @return array
     */
    public function ability(): array;
}
