<?php

namespace App\Services\Patterns\Creational\FactoryMethod\Superheroes;

use App\Services\Patterns\Creational\FactoryMethod\Superhero;

class SpiderMan implements Superhero
{
    /**
     * @return array
     */
    public function ability(): array
    {
        return [
            'shoot cobwebs',
            'spider sense',
            'climbing walls',
            'agility'
        ];
    }
}
