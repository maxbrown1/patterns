<?php

declare(strict_types=1);

namespace App\Services\Patterns\Creational\FactoryMethod\Superheroes;

use App\Services\Patterns\Creational\FactoryMethod\Superhero;

class Wolverine implements Superhero
{
    /**
     * @return array
     */
    public function ability(): array
    {
        return [
            'regeneration',
            'adamantium skeleton',
            'blades',
            'strength'
        ];
    }
}
