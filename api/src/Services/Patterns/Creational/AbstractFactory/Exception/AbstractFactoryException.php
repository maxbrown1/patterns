<?php

declare(strict_types=1);

namespace App\Services\Patterns\Creational\AbstractFactory\Exception;

use Exception;
use Throwable;

class AbstractFactoryException extends Exception
{
    public function __construct(string $message = 'Фабрика не найдена!', int $code = 404, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
