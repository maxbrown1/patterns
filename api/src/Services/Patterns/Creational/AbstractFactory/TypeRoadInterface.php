<?php

declare(strict_types=1);

namespace App\Services\Patterns\Creational\AbstractFactory;

interface TypeRoadInterface
{
    /**
     * @return string
     */
    public function road(): string;
}
