<?php

namespace App\Services\Patterns\Creational\AbstractFactory;

use App\Services\Patterns\Creational\AbstractFactory\Transport\Sea\{
    SeaTypeRoad,
    SeaTransportSubject,
    SeaRepairStation
};

class SeaTransport implements TransportFactoryInterface
{
    /**
     * @return TypeRoadInterface
     */
    public function typeRoad(): TypeRoadInterface
    {
        return new SeaTypeRoad();
    }

    /**
     * @return TransportSubjectInterface
     */
    public function transportSubject(): TransportSubjectInterface
    {
        return new SeaTransportSubject();
    }

    /**
     * @return RepairStationInterface
     */
    public function repairStation(): RepairStationInterface
    {
        return new SeaRepairStation();
    }
}