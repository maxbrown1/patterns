<?php
declare(strict_types=1);

namespace App\Services\Patterns\Creational\AbstractFactory;

interface TransportFactoryInterface
{
    /**
     * @return TypeRoadInterface
     */
    public function typeRoad(): TypeRoadInterface;

    /**
     * @return TransportSubjectInterface
     */
    public function transportSubject(): TransportSubjectInterface;

    /**
     * @return RepairStationInterface
     */
    public function repairStation(): RepairStationInterface;
}
