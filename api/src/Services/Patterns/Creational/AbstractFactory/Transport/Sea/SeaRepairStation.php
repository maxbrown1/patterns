<?php

declare(strict_types=1);

namespace App\Services\Patterns\Creational\AbstractFactory\Transport\Sea;

use App\Services\Patterns\Creational\AbstractFactory\RepairStationInterface;

class SeaRepairStation implements RepairStationInterface
{
    /**
     * @return string
     */
    public function station(): string
    {
        return 'порт';
    }
}
