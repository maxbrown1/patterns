<?php

declare(strict_types=1);

namespace App\Services\Patterns\Creational\AbstractFactory\Transport\Sea;

use App\Services\Patterns\Creational\AbstractFactory\TransportSubjectInterface;

class SeaTransportSubject implements TransportSubjectInterface
{
    /**
     * @return string
     */
    public function subject(): string
    {
        return 'корабль';
    }
}
