<?php

declare(strict_types=1);

namespace App\Services\Patterns\Creational\AbstractFactory\Transport\Sea;

use App\Services\Patterns\Creational\AbstractFactory\TypeRoadInterface;

class SeaTypeRoad implements TypeRoadInterface
{
    /**
     * @return string
     */
    public function road(): string
    {
        return 'морские транспортные линии';
    }
}