<?php

declare(strict_types=1);

namespace App\Services\Patterns\Creational\AbstractFactory;

use App\Services\Patterns\Creational\AbstractFactory\Exception\AbstractFactoryException;
use App\Services\Patterns\Creational\AbstractFactory\Transport\Sea\{
    SeaTypeRoad,
    SeaTransportSubject,
    SeaRepairStation
};

class ServiceManager
{
    public const FACTORY_MAP = [
        'sea' => SeaTransport::class,
    ];

    private string $transport;

    public function __construct($transport)
    {
       $this->transport = $transport;
    }

    /**
     * @return TransportFactoryInterface
     *
     * @throws AbstractFactoryException
     */
    public function getService(): TransportFactoryInterface
    {
        if (in_array($this->transport, array_flip(self::FACTORY_MAP))) {
            $class = self::FACTORY_MAP[$this->transport];

            return new $class;
        }

        throw new AbstractFactoryException();
    }
}