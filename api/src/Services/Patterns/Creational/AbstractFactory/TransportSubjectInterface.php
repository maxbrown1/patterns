<?php

declare(strict_types=1);

namespace App\Services\Patterns\Creational\AbstractFactory;

interface TransportSubjectInterface
{
    public function subject(): string;
}
