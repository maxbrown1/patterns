<?php

declare(strict_types=1);

namespace App\Services\Patterns\Structural\Decorator;

interface BottleInterface
{
    public function drink(): string;
}
