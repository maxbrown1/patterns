<?php

declare(strict_types=1);

namespace App\Services\Patterns\Structural\Decorator;

class BeerBottle implements BottleInterface
{
    /**
     * @return string
     */
    public function drink(): string
    {
        return __FUNCTION__ . ' beer';
    }
}
