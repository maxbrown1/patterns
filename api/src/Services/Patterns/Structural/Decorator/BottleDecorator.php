<?php

declare(strict_types=1);

namespace App\Services\Patterns\Structural\Decorator;

class BottleDecorator implements BottleInterface
{
    private BottleInterface $bottle;

    public function __construct(BottleInterface $bottle)
    {
        $this->bottle = $bottle;
    }

    /**
     * @return string
     */
    public function drink(): string
    {
        return 'I like ' . $this->bottle->drink();
    }
}
