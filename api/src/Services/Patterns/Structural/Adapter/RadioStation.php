<?php

declare(strict_types=1);

namespace App\Services\Patterns\Structural\Adapter;

class RadioStation implements PlayMusicInterface
{
    protected PlayerAdapter $playerAdapter;

    public function __construct(PlayerAdapter $playerAdapter = new PlayerAdapter())
    {
        $this->playerAdapter = $playerAdapter;
    }

    public function play(): string
    {
       return $this->playerAdapter->play();
    }
}
