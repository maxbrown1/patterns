<?php

declare(strict_types=1);

namespace App\Services\Patterns\Structural\Adapter;

class PlayerAdapter implements PlayMusicInterface
{
    public PlayerMp3 $playerMp3;

    public function __construct(PlayerMp3 $playerMp3 = new PlayerMp3())
    {
        $this->playerMp3 = $playerMp3;
    }

    /**
     * @return string
     */
    public function play(): string
    {
        return $this->convertMp3ToWAV();
    }

    /**
     * @return string
     */
    protected function convertMp3ToWAV(): string
    {
        return $this->playerMp3->melody . ' convert to WAV';
    }
}
