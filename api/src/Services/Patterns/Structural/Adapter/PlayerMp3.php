<?php

declare(strict_types=1);

namespace App\Services\Patterns\Structural\Adapter;

class PlayerMp3
{
    public string $melody;

    public function __construct(string $melody = 'mp3')
    {
        $this->melody = $melody;
    }
}
