<?php

declare(strict_types=1);

namespace App\Services\Patterns\Structural\Adapter;

interface PlayMusicInterface
{
    public function play(): string;
}
