<?php

declare(strict_types=1);

namespace App\Services\Patterns\Structural\Flyweight\Weapons;

class AK
{
    public int $damage = 150;

    public float $caliber = 5.56;

    public float $weight = 13.7;
}
