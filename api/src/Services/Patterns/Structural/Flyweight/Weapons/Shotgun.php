<?php

declare(strict_types=1);

namespace App\Services\Patterns\Structural\Flyweight\Weapons;

class Shotgun
{
    public int $damage = 250;

    public float $caliber = 8;

    public float $weight = 2.5;
}
