<?php

declare(strict_types=1);

namespace App\Services\Patterns\Structural\Flyweight\Weapons;

class Gun
{
    public int $damage = 50;

    public float $caliber = 10.0;

    public float $weight = 2.5;
}
