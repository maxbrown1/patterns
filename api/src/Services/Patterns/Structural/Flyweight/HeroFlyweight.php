<?php

declare(strict_types=1);

namespace App\Services\Patterns\Structural\Flyweight;

class HeroFlyweight
{
    private string $weapons;

    private string $chestak;

    public function __construct(string $weapons, string $chestak)
    {
        $this->weapons = $weapons;
        $this->chestak = $chestak;
    }

    /**
     * @param string $heroClass
     * @param string $chestak
     *
     * @return string
     */
    public function ammunitions(string $heroClass, string $chestak): string
    {
        $dataHero = [
            'heroClass' => $heroClass,
            'weapons' => $this->weapons,
            'chestak' => $this->chestak
        ];

        return (string) $dataHero;
    }

}
