<?php

declare(strict_types=1);

namespace App\Services\Patterns\Structural\Flyweight;

use function DI\string;

class HeroService
{
    public function execute()
    {
        $weaponsFactory = new WeaponsFlyweightFactory(['AK', 'Gun']);
        $chestakFactory = new ChestakFlyweightFactory(['SupermutantChestak']);


        $supermutant = [
            'name' => 'Grom',
            'weapon' => $weaponsFactory->getWeapons('AK'),
            'chestak' => $chestakFactory->getChestak('SupermutantChestak')
        ];

        $human = $this->addNewHero(
            $weaponsFactory,
            $chestakFactory,
            'Max',
            'HumanChestak',
            'Shotgun'
        );

        return array_merge([$supermutant], [$human]);
    }

    public function addNewHero(
        WeaponsFlyweightFactory $weaponsFlyweightFactory,
        ChestakFlyweightFactory $chestakFlyweightFactory,
        string $name,
        string $chestak,
        string $weapons
    ): array
    {
        $weaponsAttribute = $weaponsFlyweightFactory->getWeapons($weapons);
        $chestakAttribute = $chestakFlyweightFactory->getChestak($chestak);

        return [
            'name' => $name,
            'weapon' => $weaponsAttribute,
            'chestak' => $chestakAttribute,
        ];
    }
}
