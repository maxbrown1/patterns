<?php

declare(strict_types=1);

namespace App\Services\Patterns\Structural\Flyweight\Chestaki;

class HumanChestak
{
    public int $bullet = 50;

    public int $energo = 25;

    public int $radiation = 10;
}
