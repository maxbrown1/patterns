<?php

declare(strict_types=1);

namespace App\Services\Patterns\Structural\Flyweight\Chestaki;

class SupermutantChestak
{
    public int $bullet = 75;

    public int $energo = 10;

    public int $radiation = 0;
}

