<?php

declare(strict_types=1);

namespace App\Services\Patterns\Structural\Flyweight;
use App\Services\Patterns\Structural\Flyweight\Weapons\{AK, Gun, Shotgun};

class WeaponsFlyweightFactory
{
    public array $weaponsList = [];

    public function __construct(array $weaponsList)
    {
        foreach ($weaponsList as $weapons) {
            $namespace = 'App\Services\Patterns\Structural\Flyweight\Weapons\\' . $weapons;
            $this->weaponsList[$weapons] = new $namespace();
        }
    }

    /**
     * @param string $weapons
     *
     * @return string
     */
    public function getWeapons(string $weapons): string
    {
        if (!isset($this->weaponsList[$weapons])) {
            $namespace = 'App\Services\Patterns\Structural\Flyweight\Weapons\\' . $weapons;
            $this->weaponsList[$weapons] = new $namespace();
        }

        return json_encode($this->weaponsList[$weapons]);
    }
}
