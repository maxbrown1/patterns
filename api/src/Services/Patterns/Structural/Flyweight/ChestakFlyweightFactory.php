<?php

declare(strict_types=1);

namespace App\Services\Patterns\Structural\Flyweight;

use App\Services\Patterns\Structural\Flyweight\Chestaki\{HumanChestak, SupermutantChestak};

class ChestakFlyweightFactory
{
    public array $chestakList = [];

    public function __construct(array $chestakList)
    {
        foreach ($chestakList as $chestak) {
            $namespace = 'App\Services\Patterns\Structural\Flyweight\Chestaki\\' . $chestak;
            $this->chestakList[$chestak] = new $namespace();
        }
    }

    /**
     * @param string $chestak
     *
     * @return string
     */
    public function getChestak(string $chestak): string
    {
        if (!isset($this->chestakList[$chestak])) {
            $namespace = 'App\Services\Patterns\Structural\Flyweight\Chestaki\\' . $chestak;
            $this->chestakList[$chestak] = new $namespace();
        }

        return json_encode($this->chestakList[$chestak]);
    }
}
