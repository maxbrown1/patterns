<?php

declare(strict_types=1);

namespace App\Services\Patterns\Structural\Facade;

class UpperText
{
    public string $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function up(): string
    {
        return strtoupper($this->data);
    }
}
