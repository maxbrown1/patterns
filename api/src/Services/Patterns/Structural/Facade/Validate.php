<?php

declare(strict_types=1);

namespace App\Services\Patterns\Structural\Facade;

class Validate
{
    public ?string $data;

    public function __construct(?string $data)
    {
        $this->data = $data;
    }

    public function validate(): bool
    {
        return empty($this->data);
    }
}
