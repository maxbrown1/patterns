<?php

declare(strict_types=1);

namespace App\Services\Patterns\Structural\Facade;

class FacadeService
{
    public ?string $data;

    public function __construct(?string $data)
    {
        $this->data = $data;
    }

    public function facadeRun(): string
    {
        if ((new Validate($this->data))->validate()) {
            throw new \Exception('Validate exception!');
        }

        $preparingData = (new PreparingData($this->data))->preparing();

        return (new UpperText($preparingData))->up();
    }
}
