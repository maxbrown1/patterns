<?php

declare(strict_types=1);

namespace App\Services\Patterns\Structural\Facade;

class PreparingData
{
    public string $data;

    public function __construct(string $data)
    {
        $this->data = $data;
    }

    public function preparing(): string
    {
        return $this->data . ' ' . 'preparing';
    }
}
