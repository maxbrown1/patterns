<?php

declare(strict_types=1);

namespace App\Services\Patterns\Structural\Proxy;

class ProxyTimeService implements ClockInterface
{
    public ClockInterface $clock;

    public function __construct(ClockInterface $clock)
    {
        $this->clock = $clock;
    }

    /**
     * @return string
     */
    public function time(): string
    {
        return 'Current time: ' . $this->clock->time();
    }
}
