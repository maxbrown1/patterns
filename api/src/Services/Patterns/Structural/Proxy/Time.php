<?php

declare(strict_types=1);

namespace App\Services\Patterns\Structural\Proxy;

use DateTime;

class Time implements ClockInterface
{
    /**
     * @return string
     */
    public function time(): string
    {
        return (new DateTime())->format('H:i:s');
    }
}
