<?php

declare(strict_types=1);

namespace App\Services\Patterns\Structural\Proxy;

interface ClockInterface
{
    public function time(): string;
}
