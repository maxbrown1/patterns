<?php

declare(strict_types=1);

use App\Http\Action\IndexAction;
use App\Http\Action\Patterns\Creational\AbstractFactory\{
    AbstractFactoryRoadAction,
    AbstractFactorySubjectAction,
    AbstractFactoryStationAction
};
use App\Http\Action\Patterns\Creational\FactoryMethodAction;
use App\Http\Action\Patterns\Structural\AdapterAction;
use App\Http\Action\Patterns\Structural\DecoratorAction;
use App\Http\Action\Patterns\Structural\FacadeAction;
use App\Http\Action\Patterns\Structural\FlyweightAction;
use App\Http\Action\Patterns\Structural\ProxyAction;
use Slim\App;
use Slim\Routing\RouteCollectorProxy;

return static function (App $app): void {
    $app->group('', function (RouteCollectorProxy $app) {
        $app->get('/', IndexAction::class);
    });

    $app->group('/creational', function (RouteCollectorProxy $app) {
        $app->get('/factory-method', FactoryMethodAction::class);
        $app->get('/abstract-factory/road', AbstractFactoryRoadAction::class);
        $app->get('/abstract-factory/subject', AbstractFactorySubjectAction::class);
        $app->get('/abstract-factory/station', AbstractFactoryStationAction::class);
    });

    $app->group('/structural', function (RouteCollectorProxy $app) {
        $app->get('/facade', FacadeAction::class);
        $app->get('/adapter', AdapterAction::class);
        $app->get('/decorator', DecoratorAction::class);
        $app->get('/proxy', ProxyAction::class);
        $app->get('/flyweight', FlyweightAction::class);
    });
};
